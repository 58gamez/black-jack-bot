from random import shuffle

# define 
class Deck:
	"""docstring for ClassName"""
	cards = []
	ranks = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A']
			

	def __init__(self,decks):
		self.decks = decks #decks used in game
		pass
		
	#shuffle new deck(s)
	def shuffle (self):
		self.cards = []
		for i in range(4*self.decks):
			self.cards.extend(self.ranks)

		shuffle(self.cards)
		pass

	def dealCard(self):
		card = self.cards.pop(0)
		return card



class Dealer:
	def __init__(self):
		self.hand  = Hand()
		pass

	def addCard(self,card):
		self.hand.addCard(card)



class Hand:
	def __init__(self):
		self.cards = []
		self.split = False
		self.finished = False
		pass


	def addCard(self,card):
		self.cards.append(card)


	def handValue(self):
		value 	= 0
		aces 	= 0
		for card  in self.cards:
			if card == 'A':
				aces 	+= 1
				value  	+= 11
			elif card  in ['J', 'Q', 'K']:
				value  	+= 10
			else:
				value  	+= int(card)

			while (value > 21) and aces:
				value -= 10
				aces -= 1

		return value
		
	def aces(self):
		aces  =  0
		for card in self.cards:
			if card == 'A':
				aces +=1
		return aces


	def isBlackJack (self):
		if len(self.cards)==2 and self.handValue() == 21  and not  self.split:
			return True

		return False


	def canSplit (self):
		if len(self.cards) == 2 and self.cards[0]==self.cards[1]:
			return  1

		return 0
		pass


	def canDoubleDown (self):
		pass



class Player:
	def __init__(self,chips):
		self.split = False
		self.chips = chips
		# self.hand  	= Hand()
		self.hands  = []
		# self.hand2  = Hand()
		pass


	# def addCard(self,card):
	# 	self.hand.addCard(card)



	def playerSplit (self,bet):
		self.split = True
		new_hand = Hand()
		split_card = self.hands[0].cards.pop(1)
		new_hand.addCard(split_card)
		self.hands.append ( new_hand )
		self.chips -= bet

	def newHand(self):
		self.hands 		=[] 
		self.split  		= False
		self.hands.append( Hand() )

	def canAct(self):
		for hand in self.hands:
			if hand.handValue()<21 and not hand.finished:
				return True

		return False



class Game:
	player = None
	dealer = None
	debug  = True
	def __init__(self,player):
		self.player = player
		self.deck 	= Deck(6)
		self.bet 	= [0]
		self.debug  = True
		pass

	def newHand (self):
		self.player.newHand()
		self.dealer = Dealer()
		self.deck.shuffle()

	def playerBet (self,bet):
		#validate bet ??
		self.player.chips -= bet
		self.bet = [bet]

	def deal(self):
		self.player.hands[0].addCard(self.deck.dealCard())
		self.player.hands[0].addCard(self.deck.dealCard())
		self.dealer.addCard(self.deck.dealCard())

		if self.player.hands[0].isBlackJack():
			if self.debug:
				print "BLACKJACK!!!"
			self.playerStay(self.player.hands[0])



	def playerHit(self, hand):
		hand.addCard(self.deck.dealCard())
		if self.debug:
			print  "HIT CARD : ",hand.cards[len(hand.cards)-1]

		if hand.handValue() == 21:
			hand.finished  = True
			# self.playerStay ()

		if hand.handValue() > 21:
			if self.debug:
				print "PLAYER BUSTED!!!"
			hand.finished  = True
			# self.playerStay ()
			

	def playerSplit (self):
		self.player.playerSplit (self.bet[0])
		self.player.hands[0].addCard(self.deck.dealCard())
		if self.debug:
			print  "card : ",self.player.hands[0].cards[len(self.player.hands[0].cards)-1]
		self.player.hands[1].addCard(self.deck.dealCard())
		if self.debug:
			print  "card : ",self.player.hands[1].cards[len(self.player.hands[1].cards)-1]

	def playerStay (self,hand):
		hand.finished  = True
		pass



	def playerFinished (self):
		#deal second card
		self.dealer.addCard(self.deck.dealCard())
		if self.debug:
			print "DEALER CARDS: ", self.dealer.hand.cards,  "value:", self.dealer.hand.handValue()

		if self.dealer.hand.isBlackJack():
			if self.debug:
				print "DEALER BLACKJACK :("

		#dealer  game logic
		while self.dealer.hand.handValue()<17:
			self.dealer.addCard(self.deck.dealCard())
			if self.debug:
				print "DEALER CARDS: ", self.dealer.hand.cards,  "value:", self.dealer.hand.handValue()

		pass



	def calculatePayout(self):
		payout  = 0

		for  hand in self.player.hands:
			if self.debug:
				print  "player : ",hand.cards, "value:", hand.handValue()
			payout += self.calculatePayoutHands(self.dealer.hand, hand)

		return payout 
		pass



	def calculatePayoutHands(self, dealer_hand, player_hand):
		if player_hand.isBlackJack() and not dealer_hand.isBlackJack():
			return 2.5*self.bet[0]

		# push player BJ and  dealer BJ
		if player_hand.isBlackJack() and  dealer_hand.isBlackJack():
			return self.bet[0]

		#player win
		if player_hand.handValue() < 22 and player_hand.handValue() > dealer_hand.handValue():
			return 2*self.bet[0]

		#dealer bust
		if player_hand.handValue() < 22 and dealer_hand.handValue() > 21 :
			return 2*self.bet[0]

		#push
		if player_hand.handValue() < 22 and dealer_hand.handValue() == player_hand.handValue() :
			return self.bet[0]

		return 0










		

