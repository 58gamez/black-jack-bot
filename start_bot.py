from play import Play
from scrape_screen import ScrapeScreen
import pyautogui
import pytweening
import time

#record screen :
#ffmpeg -r 10   -f avfoundation -i "0:0" -crf 0 out.mp4
#ffmpeg -r 25   -f avfoundation  -capture_cursor 1 -capture_mouse_clicks 1   -i "0:0" -crf 0 play_example2.mp4

# bot game  :
# http://loadinstant.oldhavanacasino.eu:8080/usdhava/LobbySpa.Website/Default.aspx?skinId=1#

play = Play()
scrape =  ScrapeScreen()

# 	game states
#  ['finished','playing','dealer']
state = 0
card_value_map = []
handstart = time.time()
hands = 0

def  clickOn(off_set,button):
	if button == 'hit':
		pyautogui.moveTo(off_set[0]+475, off_set[1]+480, duration=0.3, tween=pytweening.easeOutElastic)
		pyautogui.click()
	elif button == 'stay':
		pyautogui.moveTo(off_set[0]+400, off_set[1]+470, duration=0.3, tween=pytweening.easeOutElastic)
		pyautogui.click()
	elif button == 'deal' or button == 'rebet':
		pyautogui.moveTo(off_set[0]+560, off_set[1]+450, duration=0.3, tween=pytweening.easeOutElastic)
		pyautogui.click()
	pass

while 1  == 1:

	dealer_cards, cards, controls = scrape.updateFrame()
	print 'Hand: ', hands
	print 'Dealer: ',dealer_cards
	print 'Player: ',cards
	print 'Controls: ',controls

	#STATE MACHINE LOGIC
	if state == 0 and ('rebet' in  controls  or 'deal' in  controls):
		print '============NEW HAND==========='
		state = 1 
		play.newHand()
		#todo press rebet  or deal
		handstart = time.time()
		clickOn(scrape.c,'deal')
		hands += 1

	# double BJ state
	
	# state  1 waiting for deal
	elif state == 1 and 'stay' in controls :
		state = 2


	#blackjack next game
	elif state == 1 and 'blackjack' in  controls and 'stay' in  controls:
		clickOn(scrape.c,'stay')
		state = 0

	elif state == 1 and 'blackjack' in  controls:
		state = 0

	elif state == 1 and 'rebet' in  controls and time.time() - handstart > 5 :
		state = 0


	elif state == 2:
		#work out  new  cards
		if 'hit' in controls:
			'state  1 and controls'
			play.setCards (cards,dealer_cards)
			action = play.getAction ()
			print 'Action', action
			clickOn(scrape.c,action)


		if 'lost' in controls  or  'bust' in controls or  'rebet' in  controls  or 'deal' in  controls:
			state = 0
			print  'Game Finished:'
			play.setCards (cards,dealer_cards)
			print 'Payout  : ', play.finishHand ()

	pass