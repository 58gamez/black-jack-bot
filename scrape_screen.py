import cv2
import numpy as np
from matplotlib import pyplot as plt
from PIL import ImageGrab
import time
# import gtk
from PIL import Image

# setup to play : http://loadinstant.oldhavanacasino.eu:8080/usdhava/LobbySpa.Website/Default.aspx?skinId=1#

class ScrapeScreen:
	def __init__(self):
		self.cards = ['2','2r','3','4','5','5b','6','6b','7','8','9','10','J','Q','K','Kb','A']
		self.controls = ['deal','rebet','stay','hit','double','bust','lost','push','timeout','blackjack']
		self.c = (0,0)
		self.card_templates  = []
		self.controls_template = []
		self.corner_reference =  cv2.imread('controls/corner-reference.png',0)
		self.card_value_map =  {'2r': '2', '5b' : '5', '6b': '6', 'Kb' : 'K'}

		for  card in self.cards:
			self.card_templates.append(cv2.imread('cards/'+card+'.png',0))

		for  control in self.controls:
			self.controls_template.append(cv2.imread('controls/'+control+'.png',0))

		pass
		

	#get corner reference
	def getReferenceCoords (self, screen):
		# output = ''
		img_gray = cv2.cvtColor(screen, cv2.COLOR_BGR2GRAY)

		# for idx, template in enumerate(card_templates):
		res = cv2.matchTemplate(img_gray,self.corner_reference,cv2.TM_CCOEFF_NORMED)
		loc = np.where( res >= 0.90)

		for pt in zip(*loc[::-1]):
			return pt
		return False
		pass


	def checkCards(self, screen):
		output = []
		img_gray = cv2.cvtColor(screen, cv2.COLOR_BGR2GRAY)

		for idx, template in enumerate(self.card_templates):
			res = cv2.matchTemplate(img_gray,template,cv2.TM_CCOEFF_NORMED)
			loc = np.where( res >= 0.90)
			for pt in zip(*loc[::-1]):
			    # output +=' '+cards[idx]
			    card  = self.cards[idx]

			    if card in self.card_value_map:
			    	card = self.card_value_map[card]

			    output.append(card)
			pass

		return output
		pass

	def checkControls(self, screen):
		output = []
		img_gray = cv2.cvtColor(screen, cv2.COLOR_BGR2GRAY)

		for idx, template in enumerate(self.controls_template):
			res = cv2.matchTemplate(img_gray,template,cv2.TM_CCOEFF_NORMED)
			loc = np.where( res >= 0.90)
			for pt in zip(*loc[::-1]):
			    output.append(self.controls[idx])
			pass

		return output
		pass


	def updateFrame(self):
		# global c
		start_time 		= time.time()
		dealer_cards 	= []
		cards  			= []
		controls 		= []



		im = ImageGrab.grab(bbox=(0,0,1000,1000))
		im = im.convert('RGB')
		open_cv_image = np.array(im) 

		#Get corner points:
		coords  = self.getReferenceCoords(open_cv_image)
		if coords != False:
			self.c = coords


		# #get dealer 
		im_board = im.crop((self.c[0]+330, self.c[1]-40, self.c[0]+510, self.c[1]+60))
		open_cv_image = np.array(im_board) 
		# open_cv_image = np.array(im) 
		# open_cv_image = open_cv_image[250:200, 520:310]
		dealer_cards = self.checkCards( open_cv_image)


		#get  hand
		im_hand = im.crop((self.c[0]+300, self.c[1]+200, self.c[0]+500, self.c[1]+300))
		# im_hand = im
		open_cv_image = np.array(im_hand) 
		cards = self.checkCards( open_cv_image)


		#check controls
		im_controls= im.crop((self.c[0]+180, self.c[1]+110, self.c[0]+610, self.c[1]+570))
		# im_hand = im
		open_cv_image = np.array(im_controls) 
		controls = self.checkControls( open_cv_image)


		print("--- %s seconds ---" % (time.time() - start_time))

		return dealer_cards, cards, controls
		pass