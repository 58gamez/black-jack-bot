#use av table to play blackjack
from blackjack import Player
from blackjack import Game
import pickle

class Play:

		def __init__(self):
			self.Q =  pickle.load( open( 'simple_av_table.p', "rb" ) )
			self.game = Game(Player(1))
			self.bet = 0.0
			self.won = 0.0
			self.hands = 0
			self.bet_ammount = 1.0
			self.newHand()
		pass

		#reset hand
		def newHand (self):
			# print '============NEW HAND==========='
			self.game.newHand()
			self.game.playerBet (self.bet_ammount)
			self.bet  += self.bet_ammount
			pass

		# # deal out cards
		# def deal (card1, card2, dealer_card):
		# 	self.game.player.addCard(card1)
		# 	self.game.player.addCard(card2)
		# 	self.game.dealer.addCard(dealer_card)
		# 	pass

		# #add player card
		# def addCard (self, card):
		# 	self.game.player.addCard(card)

		# #add dealer card
		# def addCardDealer (self, card):
		# 	self.game.dealer.addCard(card)

		def setCards (self,player_cards,dealer_cards):	
			self.game.player.hands[0].cards  = player_cards
			self.game.dealer.hand.cards  	 = dealer_cards


		def getAction (self):
	

			s1  =  self.game.player.hands[0].handValue()
			s2  =  self.game.dealer.hand.handValue()

			max_act, max_val = self.max_Q((s1,s2))
			return  max_act


		def max_Q(self, s):
			val = None
			act = None
			for a, q in self.Q[s].items():
			    if val is None or (q > val):
			        val = q
			        act = a
			return act, val


		def finishHand (self):
			self.game.playerFinished()
			payout  =  self.game.calculatePayout()
			self.won += payout
			return payout